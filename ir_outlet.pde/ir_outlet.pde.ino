/**
 * blog.ezoqc.com
 * Par Sylvain Cloutier
 * Ce script permet d'activer ou de désactiver un relais à l'aide d'une combinaison de touches
 * volume "up" et volume "down" sur une télécommande de télévision.
 */
#include <IRremote.h>

/* U pour Volume UP et D pour Volume DOWN */
const char SECRET[] = {
  'U', 'U', 'D', 'U'
};

/* Pins */
const int IR_PIN = 7;
const int RELAY_PIN = 8;

/* Signaux IR */
const int VOLUME_UP = 0x490;
const int VOLUME_DOWN = 0xc90;

/* Nombre d'entrées dans le buffer */
const int BUFFER_SIZE = 4;

/* Le buffer et l'indice actuel */
char inputBuffer[BUFFER_SIZE];
int currentBufferIdx = 0;

/* Pour le traitement du signal IR */
IRrecv irrecv(IR_PIN);
decode_results results;

/* Permet de savoir si le relais est activé sans lire l'état de la pin */
bool relayIsOn = true;

/* Les limites temporelles, en millisecondes */
long lastTimeButtonPressed = 0;
long allowedTimeBetweenPressed = 300;
long clearBufferThreashold = 1000;

void setup(){
  Serial.begin(9600);
  irrecv.enableIRIn();
  irrecv.blink13(true);

  pinMode(RELAY_PIN, OUTPUT);
}
 
void loop(){
  if (irrecv.decode(&results)){
    if (millis() - lastTimeButtonPressed > clearBufferThreashold && currentBufferIdx > 0) {
      clearBuffer();
    }
    
    if (results.value == VOLUME_UP && millis() - lastTimeButtonPressed > allowedTimeBetweenPressed) {
      lastTimeButtonPressed = millis();
      inputBuffer[currentBufferIdx] = 'U';
      currentBufferIdx++;
      Serial.println("Pushing U");
    } else if (results.value == VOLUME_DOWN && millis() - lastTimeButtonPressed > allowedTimeBetweenPressed) {
      lastTimeButtonPressed = millis();
      inputBuffer[currentBufferIdx] = 'D';
      currentBufferIdx++;
      Serial.println("Pushing D");
    }

    if (currentBufferIdx > BUFFER_SIZE - 1) {
      checkForSecret();
      clearBuffer();
    }
    
    irrecv.resume();
  }
}

void clearBuffer() {
  Serial.println("Clearing buffer");
  currentBufferIdx = 0;
}

void checkForSecret() {
  int idx = 0;
  bool allEquals = true;
  while(idx < BUFFER_SIZE - 1 && allEquals) {
    allEquals = SECRET[idx] == inputBuffer[idx];
    idx++;
  }

  if (allEquals && relayIsOn) {
    turnOffRelay();
  } else if (allEquals && !relayIsOn) {
    turnOnRelay();  
  }
}

void turnOnRelay() {
  Serial.println("Turning ON the relay");
  relayIsOn = true;
  digitalWrite(RELAY_PIN, LOW);  
}

void turnOffRelay() {
  Serial.println("Turning OFF the relay");
  relayIsOn = false;
  digitalWrite(RELAY_PIN, HIGH);
}
